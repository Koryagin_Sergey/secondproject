//
//  ViewController.swift
//  SecondProject
//
//  Created by Admin on 12.03.2021.
//

import UIKit

struct Model {
    let title: String?
//    let subtitle: String?
//    let imageName: String?
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let cellIdentifier = "cellIdentifier"
    
    @IBOutlet weak var tableVIew: UITableView!
    
    var models = [Model]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        models.append(Model(title: "Test1"))
        models.append(Model(title: "Test2"))
        models.append(Model(title: "Test3"))
        models.append(Model(title: "Test4"))
        models.append(Model(title: "Test6"))

        self.tableVIew.dataSource = self
        self.tableVIew.delegate = self
    }

    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("select row at section \(indexPath.section) row \(indexPath.row)")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    
    //MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let nCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        guard let customCell = nCell as? CustomCell else {
            return nCell
        }

        let model = self.models[indexPath.row]
        
        customCell.lblTitle.text = model.title//"cell with section \(indexPath.section) and row \(indexPath.row)"
        
//        var im = UIImageView()
//        im.image = UIImage(named: "imageName")
        
        return customCell
    }

}

